import "openzeppelin/token/ERC20/ERC20.sol";
import "openzeppelin/token/ERC20/ERC20Burnable.sol";
import "openzeppelin/token/ERC20/ERC20Detailed.sol";
import "openzeppelin/token/ERC20/ERC20Mintable.sol";

contract DoggoToken is ERC20, ERC20Detailed, ERC20Mintable, ERC20Burnable {
    constructor(
        string name,
        string symbol,
        uint8 decimals,
        address[] minters
    )
        ERC20Burnable()
        ERC20Mintable()
        ERC20Detailed(name, symbol, decimals)
        ERC20()
        public
    {}
}